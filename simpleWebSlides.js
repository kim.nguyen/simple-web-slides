var getScriptURL = (function() {
    let scripts = document.getElementsByTagName('script');
    let index = scripts.length - 1;
    let myScript = scripts[index];
    return function() { return myScript.src||""; };
})();

/*
  Namespace object.
*/

var SWS = SWS || {};

SWS.Utils = (() => {
    class Utils {
        isUndefined (o) {
            return typeof o == "undefined";
        }

        push2(stack, i, v) {
            if (!Array.isArray(stack[i])) {
                stack[i] = new Array();
            }
            stack[i].push(v);
        }

        isEmpty (o) {
            for (let _ in o) return false;
            return true;
        }
        parseFrameSpec(s) {
            let result = {};
            let min_last = 10000;
            let max_value = -1;
            for (let e of s.split("_")) {
                let bounds = e.split("-");
                if (bounds.length == 0 || bounds.length > 2) return {};
                if (bounds.length == 1) {
                    bounds[1] = bounds[0];
                }
                let a = parseInt(bounds[0]);
                let b = parseInt(bounds[1]);
                if (!isFinite(a) || !isFinite(b)) return {};
                a = Math.min(a,1000);
                b = Math.min(b,1000);
                if (b > max_value) max_value = b;
                for (let j = a; j <= b; j++)
                    result[j] = true;
            };
            return result;
        }

        getParameterByName (name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            let regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
            let results = regex.exec(location.search);
            return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        getKey(ev) {
            if (typeof ev.key == "undefined" || ev.key == null || ev.key == "") {
                return ev.which;
            } else {
                return ev.key;
            }
        }
    }

    return new Utils ();

})();


SWS.Templates = new function () {

    var self = this;
    self.helpPanel = "<div/>" ; /*"<div id='sws-help-panel-canvas'>\
<h1>Keyboard shortcuts</h1>\
<table>\
<tr ><td style='color:#f55;'>h/F1/H</td><td style='color:#f55;'> toggle help</td></tr>\
<tr><td>F2</td><td> toggle the control panel</td></tr>\
<tr><td>Left, PgUp,swipe left</td><td> previous step</td></tr>\
<tr><td>Right, PgDown, Space, swipe right</td><td> next step</td></tr>\
<tr><td>p</td><td> previous slide</td></tr>\
<tr><td>n</td><td> next slide</td></tr>\
<tr><td>Home</td><td> first slide</td></tr>\
<tr><td>End</td><td> last slide</td></tr>\
</table>\
</div>";*/

    self.controlPanel = `<div id='sws-control-panel-canvas'><div id='sws-control-panel'>
<div id='sws-control-panel-title-bar'>
<a title='Toggle fullscreen' id='sws-control-panel-fullscreen' class='sws-symbol' onclick='SWS.Presentation.toggleFullScreen();'></a>
<a title='Close panel' id='sws-control-panel-close' onclick='$(\"#sws-control-panel-canvas\").toggle();'>&#x2716;</a>
</div>
<div id='sws-control-panel-options'>
<span title='Change the aspect ratio' class='sws-symbol' >&#x1f4bb;</span><select id='sws-aspect-select' onchange='SWS.Presentation.changeAspect();'>
<option value='sws-aspect-4-3' selected='selected'>4:3</option>
<option value='sws-aspect-16-9'>16:9</option>
<option value='sws-aspect-16-10'>16:10</option>
</select>
<span title='Change the theme' class='sws-symbol'>&#x1f3a8;</span><select id='sws-theme-select' onchange='SWS.Presentation.changeTheme();'></select>
<a onclick='SWS.Presentation.openPrint()' ><span title='Open Print-Out' class='sws-symbol'>&#59158;</span></a>
</div>
<div id='sws-control-panel-navigation'>
<a title='First slide' class='sws-symbol' onclick='SWS.Presentation.goToSlide(SWS.Presentation.firstSlide());' >&#x23ee;</a>
<a title='Previous slide' class='sws-symbol' onclick='SWS.Presentation.previousSlide();SWS.Presentation.refresh();'>&#x23ea;</a>
<a title='Previous step' class='sws-symbol' style='-webkit-transform: scaleX(-1);' onclick='SWS.Presentation.previous();SWS.Presentation.refresh();'>&#x25b6;</a>
<a title='Next step' class='sws-symbol' onclick='SWS.Presentation.next();SWS.Presentation.refresh();'>&#x25b6;</a>
<a title='Next slide' class='sws-symbol' onclick='SWS.Presentation.nextSlide();SWS.Presentation.refresh();'>&#x23e9;</a>
<a title='Last slide' class='sws-symbol' onclick='SWS.Presentation.goToSlide(SWS.Presentation.lastSlide());'>&#x23ed;</a>
<br/>
<span class='sws-symbol'>&#xe4ae;</span><input type='text' id='sws-control-panel-slide-input' oninput='SWS.Presentation.goToSlide($(\"#sws-control-panel-slide-input\").val()-1);'></input><span id='sws-control-panel-total-slides'></span>
<input type='range' title='Navigate the presentation' id='sws-control-panel-navigation-bar' onchange='SWS.Presentation.navigate();' step='1'></input>
</div>
</div>
</div>`;
    self.slideActivate = function (o) {
        if (!(o.hasClass("sws-active-slide"))){
            o.removeClass("sws-inactive-slide").addClass("sws-active-slide");
        };
    };

    self.slideDeactivate = function (o) {
        if (!(o.hasClass("sws-inactive-slide"))){
            o.removeClass("sws-active-slide").addClass("sws-inactive-slide");
        };
    };

    self.slideChange = function (from, to) {
        var canvas = $(".sws-canvas");
        self.slideDeactivate($(canvas[from]));
        self.slideActivate($(canvas[to]));
    };

    self.objectActivate = function (o) {
        if (!(o.hasClass("sws-active-object"))){
            o.removeClass("sws-inactive-object").addClass("sws-active-object");
            return true;
        };
        return false;
    };

    self.objectDeactivate = function (o) {
        if (!(o.hasClass("sws-inactive-object"))){
            o.addClass("sws-inactive-object").removeClass("sws-active-object");
            return true;
        };
        return false;
    };

    self.updateFooter = function (o) {
        var footer = o.find(".sws-footer");
        if (footer.length && (footer.children("*").length == 0)) {
            var i = SWS.Presentation.getCurrentSlide();
            var cur = $( "<span class='sws-current-slide-number'>"
                         + (i + 1)
                         +"</span>");
            var sep = $( "<span class='sws-slide-num-sep' />");
            var tot = $( "<span class='sws-last-slide-number'>"
                         + (SWS.Presentation.getNumSlides())
                         +"</span>");
            footer.append(cur).append(sep).append(tot);
        };
    };
    self.updateHeader = function (o) {};
};
SWS.ConfigBuilder = function () {
    var self = this;
    self['sws-object-activate'] = SWS.Templates.objectActivate;
    self['sws-object-deactivate'] = SWS.Templates.objectDeactivate;
    self['sws-slide-change'] = SWS.Templates.slideChange;
    self['sws-update-footer'] = SWS.Templates.updateFooter;
    self['sws-update-header'] = SWS.Templates.updateHeader;
};

SWS.Defaults = new SWS.ConfigBuilder ();

SWS.Config = new SWS.ConfigBuilder ();


SWS.Effects = new function () {
    var self = this;

    self.objectDeactivateFadeOut = function (o) {
	if (o.is("embed")) return;
        o.animate({'opacity': '0'}, 200,
                  function () {

                      SWS.Templates.objectDeactivate(o);
                  });
    };

    self.objectActivateFadeIn = function (o) {

        if (SWS.Templates.objectActivate(o)){
            o.animate({'opacity': '1' }, 200);
        };

    };

    self.slideChangeHorizontalFlip = function (from, to){
        let f = SWS.Presentation.getSlide(from);
        let t = SWS.Presentation.getSlide(to);
        f.animate({ 'left': '50%', 'width': '0pt', 'opacity':'0' }, 150,
                  () => {
                      SWS.Templates.slideDeactivate(f);
                      f.css({'left':'0%', 'width': '100%'});
                      t.css({ 'left': '50%', 'width': '0pt','opacity':'0' });
                      SWS.Templates.slideActivate(t);
                      t.animate({'left':'0%', 'width': '100%','opacity':'1'});
                  });
    };
    self.slideChangeFadeOutIn = function (from, to) {
        let f = SWS.Presentation.getSlide(from);
        let t = SWS.Presentation.getSlide(to);
        f.animate({ 'opacity': '0'}, 150,
                  () => {
                            SWS.Templates.slideDeactivate(f);
                            SWS.Templates.slideActivate(t);
                            t.css('opacity', '0');
                            t.animate({ 'opacity': '1'}, 150);
                       });
    };
    self.slideChangeHorizontalSlide = function (from, to) {
        var f = SWS.Presentation.getSlide(from);
        var t = SWS.Presentation.getSlide(to);
        if (from < to) {
            t.css('left', '100%');
            t.css('opacity', '1');
            SWS.Templates.slideActivate(t);
            f.animate({ 'left': '-100%' }, 250, function () { SWS.Templates.slideDeactivate(f);
                                                              f.css('opacity', '0');
                                                              t.animate({ 'left': '0%' }, 250);
                                                            });
        } else {
            t.css('left', '-100%');
            SWS.Templates.slideActivate(t);
            f.animate({ 'left': '100%' }, 250, function () { SWS.Templates.slideDeactivate(f);
                                                             f.css('opacity', '0');
                                                           });
            t.css('opacity', '1');
            t.animate({ 'left': '0%' }, 250);
        };
    };


    self.slideChangeVerticalSlide = function (from, to) {
        var f = SWS.Presentation.getSlide(from);
        var t = SWS.Presentation.getSlide(to);
        if (from < to) {
            t.css('top', '100%');
            SWS.Templates.slideActivate(t);
            f.animate({ 'top': '-100%' }, 250, function () { SWS.Templates.slideDeactivate(f); });
            t.animate({ 'top': '0%' }, 250);
        } else {
            t.css('top', '-100%');
            SWS.Templates.slideActivate(t);
            f.animate({ 'top': '100%' }, 250, function () { SWS.Templates.slideDeactivate(f); });
            t.animate({ 'top': '0%' }, 250);
        };
    };

};

SWS.Fullscreen = new function () {
    var self = this;

    if (SWS.Utils.isUndefined(document.fullScreen)) {
        if (SWS.Utils.isUndefined(document.mozfullScreen)) {
            self.status = function () { return document.webkitIsFullScreen; };
            self.enter = function(e) {
                e.webkitRequestFullScreen();
            };
            self.exit = function () {
                document.webkitCancelFullScreen();
            };

        } else {
            self.status = function () { return document.mozfullScreen; };
            self.enter = function(e) {
                e.mozRequestFullScreen();
            };
            self.exit = function () {
                document.mozCancelFullScreen();
            };

        };
    } else {
            self.status = function () { return document.fullScreen; };
            self.enter = function(e) {
                e.requestFullScreen();
            };
            self.exit = function () {
                document.cancelFullScreen();
            };

    };


};

SWS.Presentation = new function () {


    var self = this;


    //TODO move outside of the Presentation object    let _total_slides;

    let _disable_input_events = false;
    let _initialized = false;
    let _print_mode = false;
    let _slide_callbacks = new Array ();
    let _total_steps = -1;
    let _current_theme = "";

    self.getNumSlides = function () { return _total_slides; };

    self.getSlide = function(i) {
        return $($(".sws-canvas")[i]);
    };

    self.registerCallback = function (i, f) {
        if (_initialized) return;
        //jQuery does not seem to work well
        //on a partial DOM
        let slides = $(".sws-slide");
        let h1s = $("body").children("h1");
        let slide_num = slides.add(h1s).length - 1;
        SWS.Utils.push2(_slide_callbacks, slide_num,{ 'fn': f, 'frame': i });

    };

    if (typeof(Storage)!=="undefined"){
        self.getCurrentSlide = function () {
            //unary + casts to integer
            let i = +(sessionStorage.getItem("current_slide"));
            if (!(i >= 0 && i < self.getNumSlides())){
                return 0;
            } else {
                return i;
            };
        };

        self.setCurrentSlide = function (i) {
            sessionStorage.setItem("current_slide", i);
        };

	self.showHelpAtStartup = function () {
	    let r = sessionStorage.getItem("show_help");
	    if (r == "hide") return false;
	    sessionStorage.setItem("show_help", "hide");
	    return true;
	};

    } else {
        let _current_slide = 0;
        self.getCurrentSlide = function () { return _current_slide; };
        self.setCurrentSlide = function (i) { _current_slide = i; };
	self.showHelpAtStartup = function () { return false; };
    };
    self.firstSlide = function () { return 0; };
    self.lastSlide = function () { return self.getNumSlides() - 1; };
    self.refresh = function () {
        /* block upcoming input event until all animations are finished */
        _disable_input_events = true;

        let canvas = $(".sws-canvas");
        let from_slide_num = canvas.index($(".sws-active-slide"));
        let to_slide_num = self.getCurrentSlide();
        let watch_slide_anim = false;
        let to_slide = $(canvas[to_slide_num]);
        let from_slide = $(canvas[from_slide_num]);
        let slide_change = (from_slide_num != to_slide_num);


        let info = to_slide.data("sws-frame-info");
        SWS.Config['sws-update-header'](to_slide);
        SWS.Config['sws-update-footer'](to_slide);

        if (slide_change) {
            //Launch a slide transition:
            SWS.Config['sws-slide-change'](from_slide_num, to_slide_num);
            watch_slide_anim = true;
            for (let i = 0; i < info.callbacks.at_slide.length;i++){
                info.callbacks.at_slide[i](to_slide);
            };
        };


        let cur = info.current;
        let custom = info.custom;
        let real_slide = to_slide.find(".sws-slide");
        let dont_touch = real_slide.find("sws-protect").find("*").addBack();
        real_slide.find("*").addBack().not(dont_touch).each(function (i){
            var frameset = $(this).data("sws-frame-set") || {};
            if (frameset[cur])
                SWS.Config['sws-object-activate']($(this));
            else
                SWS.Config['sws-object-deactivate']($(this));
        });


        var all = $(from_slide).add(to_slide);
        all.find("*").addBack().promise().done(function() {
            var callbacks;
            //execute callbacks when all elements are finished transitioning
            if (callbacks = info.callbacks.at_frame[self.getCurrentFrame()]){
                for (var k = 0; k < callbacks.length; k++)
                    callbacks[k]($(to_slide));
            };
            all.find("*").addBack().promise().done(function() {
                //wait for all elements to finish transitionning, in case a callback animate something
                //and enable _input_events again.
                _disable_input_events = false;
            });
        });
    };

    self.nextSlide = function () {
        self.setCurrentSlide(Math.min(self.getCurrentSlide()+1,
                                      self.lastSlide()));
        self.setCurrentFrame(self.firstFrame());
    };

    self.previousSlide = function () {
        self.setCurrentSlide(Math.max(self.getCurrentSlide()-1,
                                      self.firstSlide()));
        self.setCurrentFrame(self.firstFrame());
    };

    self.getFrameInfo = function () {

        var i = self.getCurrentSlide();
        var canvas = $($(".sws-canvas")[i]);
        var infos = canvas.data("sws-frame-info");
        return infos;
    };

    self.getTotalSteps = function () {
        if (_total_steps >= 0) return _total_steps;
        _total_steps = 0;
        $(".sws-canvas").each(function(i) {
            var canvas = $($(".sws-canvas")[i]);
            var infos = canvas.data("sws-frame-info");
            _total_steps += infos.last + 1;
        });
        return _total_steps;
    };

    self.getCurrentFrame = function () { return self.getFrameInfo().current; };
    self.setCurrentFrame = function (i) { self.getFrameInfo().current = i; };
    self.firstFrame = function () { return 0; };
    self.lastFrame = function () { return self.getFrameInfo().last; };

    self.nextFrame = function () {
        self.setCurrentFrame(Math.min(self.getCurrentFrame()+1,
                                      self.lastFrame()));

    };
    self.previousFrame = function () {
        self.setCurrentFrame(Math.max(self.getCurrentFrame()-1,
                                       self.firstFrame()));
    };

    self.next = function () {
        var i = self.getCurrentFrame();
        if (i == self.lastFrame()) {
            self.nextSlide();
            self.setCurrentFrame(self.firstFrame());
        } else
            self.nextFrame();
    };

    self.previous = function () {
        var i = self.getCurrentFrame();
        if (i == self.firstFrame()){
            self.previousSlide();
            self.setCurrentFrame(self.lastFrame());
        }
        else
            self.previousFrame();
    };

    self.goToSlide = function (s, f) {
        if (SWS.Utils.isUndefined(f))
            f = 0;
        if (!(s >= self.firstSlide() && s <= self.lastSlide())) return;
        self.setCurrentSlide(s);
        if (!(f >= self.firstFrame() && f <= self.lastFrame())) f = 0;
        self.setCurrentFrame(f);
        self.refresh();
    };

    self.cycleStyle = function() {
        var styles = $("head").children('link[rel$="stylesheet"][title]');
        var j = styles.index(styles.filter(':not(:disabled)'));
        styles[j].disabled = true;
        if (++j == styles.length) j = 0;
        styles[j].disabled = false;
    };


    self.printMode = function () {
        _print_mode = true;

	var old_fx_status = $.fx.off;
	//disable animation while printing.
        let old_title = window.title;
        window.title = "[[Printing]]";
	$.fx.off = true;
        var progress = $("<div style='position:fixed;top:0pt;left:0pt;background:white;color:black;width:100%;height:100vh;z-index:2000;' id='sws-print-progress'>Rendering presentation: <span id='sws-percent-progress'></span>%</div>");
        $("body").append(progress);

        $("html").removeClass("sws-display").addClass("sws-print");
        self.goToSlide(0,0);
        var steps = self.getTotalSteps();
        var total_steps = steps;
        var loop;
        loop = function () {
            if (steps >= 0) {
                //Crazy workaround for chromium
                ($("link.sws-theme[rel='stylesheet']")[0]).disabled = false;
                $(".sws-canvas").find("*").addBack().promise().done(function() {
                    var percent = ((total_steps - steps) / total_steps) * 100;
                    $("#sws-percent-progress").text(Math.round(percent));
                    SWS.Config['sws-slide-change'] = SWS.Templates.slideChange;
                    self.refresh();
                    $($(".sws-canvas")[self.getCurrentSlide()]).css('opacity', 1 );
                    self.next();
                    steps--;
                    loop();
                    })
            } else {
                $("#sws-percent-progress").text(100);
                progress.remove();
		window.status = 'Ready';
                window.title = old_title;
		$.fx.off = old_fx_status;

            }
        };
        loop();

    };

    self.updateConference = function () {};
    let ajaxConferenceFollower = function () {
        if (_print_mode) return;
        let fullURL = window.location.href.split("?")[0];
        let jsonURLComp = fullURL.split("/");
        let last = jsonURLComp.pop();
        let lastComp = last.split(".");
        lastComp.pop();
        lastComp.push("conf.json");
        jsonURLComp.push(lastComp.join("."));
        let jsonURL = jsonURLComp.join("/");
        function update () {
            let xhr = new XMLHttpRequest();
            console.log(jsonURL);
            xhr.open("GET", jsonURL);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    let res = JSON.parse(xhr.responseText);
                    console.log(res);
                    if (res.hasOwnProperty("slide") && res.hasOwnProperty("frame")) {
                        self.goToSlide(res.slide | 0, res.frame |0);
                        setTimeout(update, 10000);
                    }
                }
            };
            xhr.send(null);
        };
        update();

    };
    let ajaxConferenceLeader = function () {
        if (_print_mode) return;
        let fullURL = window.location.href.split("?")[0];
        let scriptURL = getScriptURL();
        let fullScriptURL = (new URL(scriptURL, fullURL)).href;
        let i = 0;
        while (true) {
            let cu = fullURL[i];
            let cs = fullScriptURL[i];
            if (cu == cs) {
                i++;
            } else {
                break;
            }
        }
        let relURL = fullURL.substring(i);
        let relScriptURL = fullScriptURL.substring(i);
        let relScriptComp = relScriptURL.split("/");
        relScriptComp.pop();
        relScriptComp = relScriptComp.map((_) => "..");
        relScriptComp.push(relURL);
        relURL = relScriptComp.join("/");
        let phpURLComp = fullScriptURL.split("/");
        phpURLComp.pop();
        phpURLComp.push("conference.php");
        let phpURL = phpURLComp.join("/");
        function update () {
            let xhr = new XMLHttpRequest();
            let request = phpURL + "?path=" + relURL + "&data=" + self.getCurrentSlide() + ":" + self.getCurrentFrame();
            console.log(request);
            xhr.open("GET", request);
            xhr.send(null);
        };
        self.updateConference = update();
    };

    let socketConferrenceFollower = function (session_id) {
        if (_print_mode) return;
        try {
            let ws = new WebSocket("wss://jupyter.nguyen.vg/sws/");
            let msg = "FOLLOWER|" + window.location.href.split("?")[0] + "|" + session_id;
            ws.addEventListener("open", function () {
                ws.send(msg);
            });
            ws.addEventListener("message", function (msg) {
                try {
                    console.log(msg);
                    let res = JSON.parse(msg.data);
                    if (res.hasOwnProperty("slide") && res.hasOwnProperty("frame")) {
                        self.goToSlide(res.slide | 0, res.frame |0);
                    }
                } catch (e) {};
            });
        } catch(e){};
    };

    let socketConferrenceLeader = function (session_id) {
        if (_print_mode) return;
        try {
            let ws = new WebSocket("wss://jupyter.nguyen.vg/sws/");
            function update () {
                let msg = "LEADER|" + window.location.href.split("?")[0] + "|" + session_id + "|" + self.getCurrentSlide() + "|" + self.getCurrentFrame();
                console.log(msg);
                ws.send(msg);
            }
            self.updateConference = update;
        } catch(e){};
    };

    if (typeof WebSocket != "undefined") {
        self.conferenceFollower = socketConferrenceFollower;
        self.conferenceLeader = socketConferrenceLeader;
    } else {
        self.conferenceFollower = () => {};
        self.conferenceLeader = () => {};
    }

    self.buildFullTOC = function (cont) {

        var build_sections = function (doc) {
            var res = [];
            var h1s = doc.find("body").first().children("h1");
            var slides = doc.find("body").first().children(".sws-slide");
            var slide_num = 1;
            var collection = h1s.add(slides);
            collection.each (function () {
                if ($(this).is("h1")) {
                    res.push({ 'title' : $(this).text(),
                               'slide' : slide_num });
                } else {
                    slide_num++;
                }
            });
            return res;
        };

        var toc = [];

        var append = function (a,e) {
            return a.push(e);
        };
        var prepend = function (a,e) {
            return a.unshift(e);
        };

        var loop = function (doc, dir, add, ignoreFirst) {

            if (ignoreFirst !== true) {
                let title =  doc.find("title").first().text();
                var this_toc = { 'title' :title
				 .replace ("&", "&amp;")
				 .replace("'","&apos;")
				 .replace('"', "&quot;")
				 .replace("<", "&lt;")
				 .replace(">", "&gt;"),
                                 'sections' : build_sections(doc) };
                add(toc, this_toc);
            };

            var url = doc.find(dir).first().attr("href");
            console.log("Found url : ", url);
            if (!SWS.Utils.isUndefined(url) && url != "") {
                let xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                    let doc;
                        try {
                            if (xhr.responseXML) {
                                doc = $(xhr.responseXML);
                            } else {
                                let xml = (new DOMParser()).parseFromString(xhr.responseText, "text/html");
                                doc = $(xml);
                            }
                    } catch (e) {
                      console.log(e);
                    };
                    loop(doc, dir, add, false);
                    }
                };
                xhr.open("GET", url);
                xhr.send();
            } else {
                cont(toc);
            }
        };
        loop ($(document), ".sws-previous", prepend, false);

    };



    var _startTouch = null;

    self.inputHandler = function (event) {
        if (_disable_input_events || _print_mode) return;
        var code = 0;
        switch (event.type) {
        case 'touchstart':
	    _startTouch = event.changedTouches[0];
            return;
        case 'touchend':
	    if (!_startTouch) return;
	    var _endTouch = event.changedTouches[0];
	    var Xdist = _endTouch.clientX - _startTouch.clientX;
	    var Ydist = _endTouch.clientY - _startTouch.clientY;
	    if (Xdist > 40) code = 39
	    else if (Xdist < -40) code = 37
            else if (Ydist > 20 && !$("#sws-control-panel-canvas").is(":visible")) code = 67;
	    else if (Ydist < -20  && $("#sws-control-panel-canvas").is(":visible")) code = 67;
	    else code = 39;
            break;
        case 'keydown':
            code = SWS.Utils.getKey(event);
            break;
        default:
            return;
        };
        switch (code) {
        case 36:/* Home */
        case "Home":
            self.setCurrentSlide(self.firstSlide());
            break;

        case 35:/* End */
        case "End":
            self.setCurrentSlide(self.lastSlide());
            break;
        case 32: /* space */
        case " ":
        case 34: /* PgDown */
        case "PageDown":

	case 38: /* Up */
	case "ArrowUp":
        case 39: /* -> */
        case "ArrowRight":
        case 38:
        case "ArrowUp":
	case 176: /* Multimedia skip forward */
        case "MediaTrackNext":
	case 179: /* Multimedia play/pause */
        case "MediaPlay":
            if (self.getCurrentSlide() == self.lastSlide()
                && self.getCurrentFrame() == self.lastFrame()) return;
            self.next();
            break;
        case 78: /* n */
        case "n":
        case "N":
            self.nextSlide();
            break;
        case 8: /* backspace */
        case "Backspace":
        case 33: /* PgUp */
        case "PageUp":
        case 37: /* <-   */
        case 40:
        case "ArrowDown":
        case "ArrowLeft":
	case 40: /* down */
	case "ArrowDown":
	case 177: /* Multimedia skip backward */
        case "MediaTrackPrevious":
            self.previous();
            break;
        case 80: /* p */
        case "p":
        case "P":
            self.previousSlide();
            break;
        case 83: /* s */
        case "s":
        case "S":
                self.cycleStyle();
            return;
        case 113: /* f2 */
        case "F2":
            $("#sws-control-panel-canvas").toggle();
	    return;
        case 72: /* h */
        case "H":
        case "h":
        case 112:
        case "F1":
            $("#sws-help-panel-canvas").toggle();

        default:
            return;
        };
        self.refresh();
        self.updateConference();
};



    function init_canvas(canvas, custom) {
        var cur_frame = 0;
        var last_frame = canvas.find(".sws-pause").length;
        //Add all regular elements to the frame list
        var slide = $(canvas.find(".sws-slide")[0]);

        var callbacks = { at_slide : new Array(),
                          at_frame : new Array() };

        if (SWS.Utils.isUndefined(custom)) {
            custom = new Array ();
        };

        for (var i = 0; i < custom.length; i++) {
            if (isFinite(custom[i].frame)){
                var num = +(custom[i].frame);
                if (num > last_frame) last_frame = num;
                SWS.Utils.push2(callbacks.at_frame, num, custom[i].fn);
            } else if (custom[i].frame == "slide")
                callbacks.at_slide.push(custom[i].fn);
            else {
                var frame_set = SWS.Utils.parseFrameSpec(custom[i].frame);
                for(var f in frame_set){
                    if (+f > +last_frame) last_frame = +f;
                    SWS.Utils.push2(callbacks.at_frame, +(f), custom[i].fn);
                };
            }
        };

        var specials = $([]);

        slide.find('*[class*="sws-onframe-"]').each(function(_){
            var cls = $(this).attr('class');
            var idx = cls.indexOf("sws-onframe-");
            if (idx >= 0) {
                var end = cls.indexOf(" ", idx);
                end = (end == -1) ? cls.length : end;
                var spec = cls.substring(idx+12, end);
                var o = SWS.Utils.parseFrameSpec(spec);
                console.log(o);
                for(var f in o)
                    if (+f > +last_frame) last_frame = +f;
                $(this).find("*").addBack().each(function(_){
                    if (!SWS.Utils.isEmpty(o)){
                        $(this).data("sws-frame-set", o);
                    }
                    specials = specials.add($(this));
                });
            };
        });

        slide.find("*").addBack().not(specials).each(function(i) {
            if ($(this).hasClass("sws-pause"))  cur_frame++;
            var o = {};
            for (var j = cur_frame; j <= last_frame; j++)
                o[ j ] = true;
            if (!SWS.Utils.isEmpty(o))
                $(this).data("sws-frame-set", o);
        });

        canvas.data("sws-frame-info", { current: 0,
                                        last: (last_frame - 0), // force cast to integer
                                        callbacks: callbacks
                                      });

    };

    /* Forces redrawing the page without reloading */
    self.redraw = function (f) {
        if (SWS.Utils.isUndefined(f))
            $("body").hide().show(400, function () {
                $("body").css("display","block");
                if (!SWS.Utils.isUndefined(f))
                    f();
            });
    };
    self.changeAspect = function() {
	if (_print_mode) return;
	var newClass = $("#sws-aspect-select").val();
	var args = newClass.split("-");
	var targetRatio = (args[2] - 0) / (args[3] - 0);
	var realRatio = window.innerWidth / window.innerHeight;
	var byClass = (targetRatio > realRatio ) ? "sws-by-height" : "sws-by-width";
	if ($("html").hasClass(newClass)
	    && $("html").hasClass(byClass))
	    return;

        $("html").removeClass("sws-aspect-4-3")
            .removeClass("sws-aspect-16-9")
            .removeClass("sws-aspect-16-10")
	    .removeClass("sws-by-width")
	    .removeClass("sws-by-height")
            .addClass(newClass).addClass(byClass);
        self.redraw();
    };

    self.getCurrentTheme = function () {
        var l = $("link.sws-theme[rel='stylesheet']")[0];

        if (l) {
            return  l.title;
        } else
            return ""
    };

    self.changeTheme = function (name) {
        var theme_name;
        if (typeof name === 'undefined')
            theme_name = $("#sws-theme-select").val()
        else
            theme_name = name;

        _current_theme = theme_name;
        $("link.sws-theme").each (function (i) {
            var e = this;
            var title =  e.title;
            if (title == theme_name) {
                e.rel = "stylesheet";
                e.disabled = false;
                e.media="all";
            } else {
                e.rel = "alternate stylesheet";
                e.disabled = true;
                e.media="all";
            };
        });
        self.redraw();

    };

    self.openPrint = function () {
        window.open ("?mode=print&theme=" + self.getCurrentTheme());
    }
    var _fullscreen_icon_on = "&#xe746;";
    var _fullscreen_icon_off = "&#xe744;";

    self.toggleFullScreen = function () {
        if (SWS.Fullscreen.status()) {
            SWS.Fullscreen.exit();
            $("a#sws-control-panel-fullscreen")
                .html(_fullscreen_icon_off);



        } else {
            SWS.Fullscreen.enter($("body")[0]);
            $("a#sws-control-panel-fullscreen")
                .html(_fullscreen_icon_on);
        };
    };
    function _update_ui() {
        var nav = $('#sws-control-panel-navigation-bar');
        nav.val(SWS.Presentation.getCurrentSlide() + 1);
        $('#sws-control-panel-slide-input').val(nav.val());
    }
    self.navigate = function () {
        self.goToSlide($("#sws-control-panel-navigation-bar").val()-1);
        _update_ui();
    };


    self.init = function () {


        $("html").addClass("sws-display");
        //$(window).resize(self.redraw);

        let slides = $(".sws-slide");
        let h1s = $("body").children("h1");
        let slide_num = slides.add(h1s).length - 1;

        _total_slides = $(".sws-slide").add($("body").children("h1")).length;

        let cur = self.getCurrentSlide();
        self.buildFullTOC(function (toc) {
            let common_html = "<div class='sws-slide sws-toc'><h1>Plan</h1><ul style='list-style-type:none'>";
            let i;
            for (i= 0; i < toc.length - 1; i++)
                common_html += "<li class='done'>" + (i+1) +
                ' ' + toc[i].title + "</li>";

            common_html += "<li>" + toc.length + ' ' + toc[toc.length - 1].title;
            common_html += "<ul style='list-style-type:none' >";
            var sections = toc[toc.length - 1].sections;
            $("body").children("h1").each(function (i) {
                let this_html = common_html;
                let j;
                let secnum = toc.length + '.';
                for (j = 0; j < i; ++j)
                    this_html += "<li class='done'>" + secnum + (j+1) + ' ' +
                    sections[j].title + "</li>";
                this_html += "<li class='hl'>" + secnum + (i+1) + ' ' +
                    sections[i].title + "</li>";
                for (j = i+1; j < sections.length; j++)
                    this_html += "<li>" + secnum + (j+1) + ' '
                    +sections[j].title + "</li>";
                this_html += "</ul></li></ul></div>";
                let this_html_obj = $(this_html);
                $(this).after(this_html);
            });

        $(".sws-slide").each(function (i) {
            var par = $(this).parent();


            $(this).remove();
            var canvas = $('<div class="sws-canvas"/>');

            if (!($(this).hasClass("sws-option-noheader"))) {
                canvas.append($('<div class="sws-header"/>'));
            };

            if (!$(this).hasClass("sws-cover")) {
                var title = $($(this).find("h1")[0]);
                var title_div = $('<div class="sws-title" />');
                title_div.append(title);
                canvas.append(title_div);
            };

            var inner = $('<div class="sws-inner-canvas"/>');
            var content = $('<div class="sws-content"/>');
            $(this).find('script[type="text/javascript"]').remove();
            content.append($(this));
            inner.append(content);
            canvas.append(inner);
	    var that = this;
	    [ "sws-cover", "sws-toc" ].forEach(
		function(v) {
		    if ($(that).hasClass(v)) {
			inner.addClass(v);
			$(that).removeClass(v);
		    }
		});

            if (!($(this).hasClass("sws-option-nofooter"))) {
                canvas.append($('<div class="sws-footer"/>'));
            };

            par.append(canvas);
            if (i == cur) {
                canvas
                    .addClass("sws-active-slide")
                    .removeClass("sws-inactive-slide");
            } else {
                canvas
                    .addClass("sws-inactive-slide")
                    .removeClass("sws-active-slide");
            };
            init_canvas(canvas,_slide_callbacks[i]);

        });

        // Initialize the control panel
        $("body").append($(SWS.Templates.controlPanel));
        $("body").append($(SWS.Templates.helpPanel));
        // Fill the theme switcher
        $("link.sws-theme").each (function (i) {
            var e = $(this);
            var opt = "<option value='";
            opt += e.attr("title");
            opt += "' ";
            if (e.attr("rel") == "stylesheet") {
                opt+= "selected='selected'";
            };
            opt += ">" + e.attr("title") + "</option>";
            $("#sws-theme-select").append($(opt));
        });
        // Set the fullscreen icon
        if (SWS.Fullscreen.status()) {
            $("a#sws-control-panel-fullscreen")
                .html(_fullscreen_icon_on);
        } else {
            $("a#sws-control-panel-fullscreen")
                .html(_fullscreen_icon_off);
        };
        // Put the navigation range at the correct position
        var nav = $('#sws-control-panel-navigation-bar');
        nav.attr("min", SWS.Presentation.firstSlide() + 1);
        nav.attr("max", SWS.Presentation.lastSlide() + 1);
        $('#sws-control-panel-total-slides').text('/' + SWS.Presentation.getNumSlides());
        _update_ui();
        _slide_callbacks = null; /* avoid a leak */
        var passed_theme = SWS.Utils.getParameterByName("theme");


        //workaround weird chrome CSS loading bug
        var f =
            function () {
                if (passed_theme == "")
                    self.changeTheme();
                else
                    self.changeTheme(passed_theme);
                if (SWS.Utils.getParameterByName("mode") == "print") {
                    self.printMode();
                } else {
                    session_id = Number.parseInt(SWS.Utils.getParameterByName("session_id"));
                    if (Number.isInteger(session_id) && session_id >= 0) {
                        if (SWS.Utils.getParameterByName("mode") == "follower") {
                            self.conferenceFollower(session_id);
                        } else if (SWS.Utils.getParameterByName("mode") == "leader") {
                            self.conferenceLeader(session_id);
                        }
                    } else if (self.showHelpAtStartup()) $("#sws-help-panel-canvas").show().delay(5000).hide();
		    self.changeAspect();
                    self.refresh();
		};
                $(document).keydown(self.inputHandler);
                document.body.addEventListener('touchstart',self.inputHandler, false);
                document.body.addEventListener('touchend',self.inputHandler, false);
		$(window).resize(self.changeAspect);
                _initialized = true;
            };
        //setTimeout(f, 100);
    f();
    });

    };

};
