

const keywords = {};
keywords.qsl = [
    { re : '(?://.*(?:\n|$))', class : 'kw-comment' },
    { re : '(?:"[^"]*")', class : 'kw-string'},
    { re : '\\b(?:function|if|else|while|fun|return|null)\\b', class : 'kw-keyword' },
    { re : '[=.:]', class : 'kw-keyword' }

];
keywords.dtd = [
    { re : '\\b(?:ELEMENT|ATTLIST|ANY|EMPTY|REQUIRED|IMPLIED|ID|IDREF|PCDATA|CDATA|FIXED)\\b', class :  'kw-keyword' },
    { re : '[*+,#|?]', class : 'kw-keyword' }
];

keywords.java = [
    { re : '(?://.*(?:\n|$))', class : 'kw-comment' },
    { re : '\\b(?:class|while|return|synchronized|if|else|private|new|try|catch|final|finally|implements|interface|extends|public|throws|static|int)\\b', class :  'kw-keyword' },
    { re : '(?:[@][a-zA-Z]+)', class :  'kw-string' },
    { re : '(?:"[^"]*")', class : 'kw-string'}

];
keywords.js = [
    { re : '\\b(?:class|await|async|import|export|return|this|super|constructor|of|var|let|const|new|throw|try|default|function|catch|if|else|for|switch|case|in|while|do|delete|instanceof|typeof|null|undefined)\\b', class :  'kw-keyword' },
    { re : '(?:["\'`](?:[^"\'`\\\\]|\\\\.)*["\'`])', class : 'kw-string'},
    { re : '(?://.*(?:\n|$))', class : 'kw-comment' }

];
keywords.shell = [
    { re : '[$>|&;]', class : 'kw-keyword' },
    { re : '\\b(?:export|if|then|else|fi|for|in|end)\\b', class :  'kw-keyword' },
    { re : '(?:["\'`](?:[^"\'`\\\\]|\\\\.)*["\'`])', class : 'kw-string'},
    { re : '(?:#.*(?:\n|$))', class : 'kw-comment' }

];

keywords.python = [
    { re : '[$>|&;]', class : 'kw-keyword' },
    { re : '\\b(?:with|class|__init__|del|def|as|return|lambda|while|import|from|for|in|not|is|and|or|for|if|else)\\b', class :  'kw-keyword' },
    { re : '(?:_(?!\w))|\\[|\\]|&lt;|&gt;|[:+*/=;-]', class : 'kw-keyword' },
    { re : '(?:["](?:[^"\\\\]|\\\\.)*["])', class : 'kw-string'},
    { re : '(?:[\'`](?:[^\'\\\\]|\\\\.)*[\'])', class : 'kw-string'},
    { re : '(?:[`](?:[^`\\\\]|\\\\.)*[`])', class : 'kw-string'},
    { re : '(?:#.*(?:\n|$))', class : 'kw-comment' }

];

keywords.ocaml = [
    { re : '(?:;;)', class : 'kw-comment'},
    { re : '\\b(?:with|exception|try|type|match|let|rec|fun|if|of|then|else|open|mod|and|in|int|float|bool|list|string|char|begin|val|end|struct|sig|module|include)\\b', class :  'kw-keyword' },
    { re : '(?=[^A-Za-z0-9])(?:\\[|\\]|<|>|-&gt;|[:+*/=_;|,-][.]?)+(?=[^A-Za-z0-9])', class : 'kw-keyword' },
    { re : '(?:["](?:[^"\\\\]|\\\\.)*["])', class : 'kw-string'},
    { re : '(?:[\'](?:[^\'\\\\]|\\\\.)[\'])', class : 'kw-string'},
    { re : '(?:[(][*](?:[^*]|[*][^)])*[*][)])', class : 'kw-comment'}


];


keywords.scala = [
    { re : '(?://.*(?:\n|$))', class : 'kw-comment' },
    { re : '\\b(?:class|val|var|object|def|Int|String|Double|if|else|while|for|<-)\\b', class :  'kw-keyword' },
    { re : '(?:_(?!\w))|\\[|\\]|&lt;|&gt;|[:+*/=;-]', class : 'kw-keyword' },
    { re : '(?:[@][a-zA-Z]+)', class :  'kw-string' },
    { re : '(?:"[^"]*")', class : 'kw-string'}

];

keywords.sql = [
    { re : '(?:--.*(?:\n|$))', class : 'kw-comment' },
    { re : '\\b(?:ALL|AND|ASC|BEGIN|BY|CASCADE|COMMIT|CONSTRAINT|CREATE|DELETE|DESC|DISTINCT|DROP|END|EXCEPT|FOREIGN|FROM|FUNCTION|GROUP|HAVING|JOIN|IN|SELECT|INSERT|INTEGER|INTERSECT|IS|ISOLATION|KEY|LEVEL|LIKE|NOT|NULL|ON|DATE|OR|ORDER|PRIMARY|REFERENCES|REPLACE|TABLE|TRANSACTION|UNION|UNIQUE|UPDATE|VALUES|WHERE)\\b', class : 'kw-keyword' },
    { re : '\\bVARCHAR[(][0-9]*[)](?:\\B|\\b)', class : 'kw-keyword' },
    { re : '(?:"[^"]*")', class : 'kw-string'}

];
keywords["c#"]= [
    { re : '(?://.*(?:\n|$))', class : 'kw-comment' },
    { re : '\\b(?:var|from|where|return|select|new|in|foreach)\\b', class : 'kw-keyword' },
    { re : '(?:"[^"]*")', class : 'kw-string'}

];
keywords["c"]= [
    { re : '(?://.*(?:\n|$))', class : 'kw-comment' },
    { re : '\\b(?:void|int|while|return)\\b', class : 'kw-keyword' },
    { re : '[*+=!-]', class : 'kw-keyword' },
    { re : '(?:"[^"]*")', class : 'kw-string'}

];
keywords["qir"]= [
    { re : '\\b(?:if|then|else|ldestr)\\b', class : 'kw-keyword' },
    { re : '[■.(),λ·:\|\\{\\}@\\[\\]]', class : 'kw-keyword' },
    { re : '\\b(?:Project|Scan|Filter|Group|Join|Limit|O)\\b', class : 'kw-operator' }
];

keywords["fakeasm"]=[
    { re : '\\b(?:load|read|write|jump|jgt|push|pop|add)\\b', class :  'kw-keyword' },
    { re : '(?:0x[0-9a-fA-F]*)', class : 'kw-string'},
    { re : '(?:#.*(?:\n|$))', class : 'kw-comment' }
];

$(window).load(function () {
    $(".sws-highlight").each(function (i, elem) {
        var lang = elem.getAttribute("data-syntax");
        let debug = elem.classList.contains("debug");
        if (lang && keywords[lang]) {
            let rules = keywords[lang].map ((r) => {
                console.log(r.re);
                return ({ re : (new RegExp( "" + r.re + "", "m")),
                          class : r.class })});
            function loop (node, parent) {
                if (node == null) return;
                if (node.nodeType == Node.TEXT_NODE || node.nodeType == Node.CDATA_SECTION_NODE) {
                    let nextSibling = node.nextSibling;
                    let text = node.nodeValue;
                    let todo = true;
                    while (todo) {
                        todo = false;
                        let matchings = rules.map ((rule) => {
                            let res = rule.re.exec(text);
                            if (res != null) {
                                return { idx : res.index,
                                         matched : res[0],
                                         class : rule.class
                                        };
                            } else {
                                return null;
                            }
                        });
                        matchings = matchings.filter((x) => x != null);
                        if (matchings.length == 0) break;
                        matchings.sort((res1, res2) => {
                            if (res1.idx < res2.idx) return -1;
                            else if (res1.idx > res2.idx) return 1;
                            else if (res1.matched.length < res2.matched.length) return 1
                            else if (res1.matched.length > res2.matched.length) return -1;
                            else return 0;
                        });
                        console.log(matchings);
                        let clss = matchings[0].class;
                        let matched = matchings[0].matched;
                        let idx = matchings[0].idx;
                        let unmatched = text.substring(0,idx);
                        let newText = document.createTextNode(matched);
                        let newHL = document.createElement("span");
                        newHL.classList.add(clss);
                        newHL.appendChild(newText);
                        if (unmatched.length > 0) parent.insertBefore(document.createTextNode(unmatched), node);
                        parent.insertBefore(newHL, node);
                        text = text.substring(idx + matched.length);
                        todo = true;

                    }
                    if (text.length == 0) {
                        parent.removeChild(node);
                    } else {
                        parent.replaceChild(document.createTextNode(text), node);
                    }
                    loop (nextSibling, parent);
                } else {
                    loop (node.firstChild, node);
                    loop (node.nextSibling, parent);
                }
            }
            if (elem.innerHTML.startsWith("\n")) {
                elem.innerHTML = elem.innerHTML.substring(1);
            }
            loop (elem.firstChild, elem);




                /* keywords[lang].forEach(function (e) {
                let re = new RegExp( "(" + e.re + ")", "gm");

                var text = elem.innerHTML.split (/(<[^>]*>|\n)/);
                if (debug) {
                    console.log('---------');
                    console.log(elem.innerHTML);
                    console.log('--');
                    console.log(text.map(n=>n));
                    console.log(re);
                }
                for (var i = 0; i < text.length; i++) {
                    if (text[i] && text[i][0] == '<') {
                        if (debug )
                            console.log('skipping', text[i]);
                        continue;
                    }
                    let old = text[i];
                    text[i] = text[i].replace (re , "<kbd class='" + e.class + "'>$1</kbd>");
                    if (debug) console.log(old, ' -> ', text[i]);
                }

                var newHTML = text.join("") ;
                elem.innerHTML = newHTML ;
                 });
           */
            // var strs = elem.getElementsByClassName('kw-string');
            // for (var i = 0; i < strs.length; i++)
            //     strs[i].innerHTML = strs[i].innerHTML.replace(/[ ]/g,'␣');
        }
    });


});
/*
$(window).load(function () {
    $("code").each(function(i, elem) {
        var HTML = elem.innerHTML;
        if (HTML && HTML[0] == "\n")
            elem.innerHTML = HTML.substring(1);
        var prev = elem.previousSibling;
        if (prev && prev.nodeType == prev.TEXT_NODE) {
            var i = prev.nodeValue.lastIndexOf("\n");
            if (i >= 0) {
                var len = prev.nodeValue.length - i;
                var texts = elem.innerHTML.split('\n');
                elem.innerHTML = texts.map (function (t) {
                    var prefix = t.substring(0,len+1);
                    if (prefix.match(/^[ ]+$/)) {
                        t = t.substring(len+1);
                    }
                    return t;
                }).join("\n");


            };


        };



    });



});
*/
