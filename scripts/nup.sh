#!/bin/sh

INPUT="$1"
OUTDIR=$(dirname "$INPUT")
if [ -z "$OUTDIR" ]; then
    OUTDIR="."
fi
OUTPUT="$OUTDIR"/$(basename "$INPUT" .pdf)"_print.pdf"
TMP="$OUTDIR"/tmp.pdf
rm -rf "$TMP" "$OUTPUT"
pdfjam  --landscape --nup 2x2 --scale 0.95 --paper a4paper -o "$TMP" "$INPUT"
# gs -sOutputFile="$OUTPUT"  -sPAPERSIZE=a4  -sDEVICE=pdfwrite -sPAPERSIZE=a4  -dCompatibilityLevel=1.3  \
#    -dDEVICEWIDTHPOINTS=841  \
#    -dDEVICEHEIGHTPOINTS=545 \
#  -dNOPAUSE -dQUIET -dBATCH -sPAPERSIZE=a4  "$TMP"
cp "$TMP" "$OUTPUT"
rm -f "$TMP"
