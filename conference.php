<?php
function endsWith($haystack, $needle) {
    return substr_compare($haystack, $needle, -strlen($needle)) === 0;
}

header('Content-Type: application/json');
function error($msg) {
    echo '{ "status" : "error", "data" : "' . $msg . '" }';
    exit (0);
}

function ok($msg) {
    echo '{ "status" : "ok", "data" : "' . $msg . '" }';
    exit (0);
}

$path = "";
if (!isset($_GET["path"])) {
    error("no path");
} else {
    $path = $_GET["path"];
}
if (!file_exists($path)) {
    error("invalid path ${path}");
}
if (endsWith($path, ".html")) {
    $output = substr($path, 0, -5) . ".conf.json";
} else if (endsWith($path, ".xhtml")) {
    $output = substr($path, 0, -6) . ".conf.json";
} else {
    error("invalid extension ${path}");
}

if (!isset($_GET["data"])) {
    error("invalid data");
}

$data = $_GET["data"];

if ($data === "remove") {
    if (file_exists($output)) {
        unlink($output);
        ok("removed");
    } else {
        error("no output file");
    }
}
if (1 === preg_match("/^[0-9]{1,5}:[0-9]{1,5}$/", $data)) {
    $tab = explode(":", $data);
    $json = '{ "slide" : ' . $tab[0] . ', "frame" : ' . $tab[1] . ' }';
    if (FALSE === file_put_contents($output, $json)) {
        error("can't write to $output, " . getcwd());
    } else {
        ok("$data");
    }
}

?>